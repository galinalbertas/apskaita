from __future__ import print_function
import RPi.GPIO as GPIO
from datetime import datetime, timedelta
import sys
import paho.mqtt.publish as publish

v = 0.0
v_init = 0.0
time_stamp_old = datetime.now()
time_stamp = datetime.now()
old_input = 1
tariff = 0.36
last_interrupt_time = datetime.now()

if len(sys.argv) == 2:
    print
    'gas counter value:', str(sys.argv[1])
    v = float(sys.argv[1])
    v_init = v


def process_counter_tick():
    global v, time_stamp, time_stamp_old
    v += 0.01
    time_stamp = datetime.now()
    delta_time = time_stamp - time_stamp_old

    volume_hour = 0.01 / (delta_time.seconds + delta_time.microseconds / 1000000.) * 3600.
    power = volume_hour * 9300.
    price_hour = volume_hour * tariff

    print_tick(delta_time, power, price_hour, time_stamp, v, volume_hour)
    publish.single("Gas", v, hostname="localhost")
    save_cdr(power, time_stamp, v, volume_hour)

    time_stamp_old = time_stamp


def print_tick(delta_time, power, price_hour, time_stamp, v, volume_hour):
    print(
        time_stamp, ' ',
        delta_time, ' dt ',
        v, ' m3 ',
        round(v - v_init, 2), ' m3 ',
        round(volume_hour, 2), ' m3/h ',
        round(power, 0), ' w ',
        round(price_hour, 2), ' Eur')


def save_cdr(power, time_stamp, v, volume_hour):
    f = open('gas.cdr', 'a')
    f.write(str(time_stamp) +
            ',' +
            str(v) +
            ',' +
            str(round(v - v_init, 2)) +
            ',' +
            str(round(volume_hour, 2)) +
            ',' +
            str(round(power, 0)) +
            '\n')
    f.close


def tick_callback(ch):
    global last_interrupt_time
    interrupt_time = datetime.now()
    # debouncing
    if interrupt_time - last_interrupt_time > timedelta(milliseconds=200):
        process_counter_tick()
        last_interrupt_time = interrupt_time


channel = 13
GPIO.setmode(GPIO.BOARD)
GPIO.setup(channel, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(channel, GPIO.FALLING, callback=tick_callback,
                      bouncetime=200)

message = raw_input("press enter to stop")
GPIO.cleanup()
