#!/usr/bin/python
from __future__ import print_function
import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import time
from datetime import datetime
import spidev
import sys
import paho.mqtt.publish as publish

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

electric_counter_value = 0.0
electric_counter_init = 0.0  # initial electric counter value
heat_pump_power_factor = 5.85
old_time_stamp = datetime.now()
old_rotation = 0
first_run_flag = 1
old_heat_pump_tick_time = 0
old_counter_tick_time = 0
radio = NRF24(GPIO, spidev.SpiDev())


def radio_setup():
    pipes = [[0xf0, 0xf0, 0xf0, 0xf0, 0xd2], [0xf0, 0xf0, 0xf0, 0xf0, 0xe1]]
    radio.begin(0, 25)
    time.sleep(1)
    radio.setRetries(15, 15)
    radio.setPayloadSize(8)
    radio.setChannel(0x4c)
    radio.setDataRate(NRF24.BR_2MBPS)
    radio.setPALevel(NRF24.PA_MAX)
    radio.setAutoAck(True)
    radio.openWritingPipe(pipes[0])  # main communication channel
    radio.openReadingPipe(1, pipes[1])  # heat pump
    radio.openReadingPipe(2, [0xaa])  # main electricity counter
    radio.openReadingPipe(3, [0xbb])
    radio.openReadingPipe(4, [0xcc])
    radio.openReadingPipe(5, [0xdd])


def print_radio_settings():
    radio.startListening()
    radio.stopListening()
    radio.printDetails()


radio_setup()
print_radio_settings()
radio.startListening()

if len(sys.argv) == 2:
    print('electric counter value:', str(sys.argv[1]))
    electric_counter_value = float(sys.argv[1])
    electric_counter_init = electric_counter_value


def save_heat_pump_cdr():
    file = open('heatpump.cdr', 'a')
    file.write(
        str(datetime.now()) +
        ',' +
        str(battery_voltage) +
        ',' +
        str(voltage_in_current_sensing_coil) +
        ',' +
        str(voltage_in_current_sensing_coil * heat_pump_power_factor) +
        '\n')
    file.close


def save_counter_cdr():
    file = open('electricity.cdr', 'a')
    file.write(
        str(time_stamp) +
        ',' +
        str(rotation_from_controller) +
        ',' +
        str(power) +
        ',' +
        str(electric_counter_value) +
        ',' +
        str(electric_counter_init) +
        ',' +
        str(electric_counter_value - electric_counter_init) +
        '\n')
    file.close


def print_counter_tick():
    print(
        'Rot:', rotation_from_controller,
        'Time:', counter_time_stamp_from_controller,
        'Delta:', counter_time_stamp_from_controller - old_counter_tick_time, '|',
        time_stamp,
        'Delta:', delta.seconds * 1000000 + delta.microseconds,
        'Power:', round(power, 0), 'w',
        'Counter:', electric_counter_value,
        'Init:', electric_counter_init,
        'Consumed:', round(electric_counter_value - electric_counter_init, 2)
    )


def print_heat_pump_tick():
    print(
        'Battery', battery_voltage, 'V',
        'Voltage', voltage_in_current_sensing_coil, 'V',
        'Power', voltage_in_current_sensing_coil * heat_pump_power_factor, 'w',
        'Time', time_stamp_from_controller,
        'Time delta', time_stamp_from_controller - old_heat_pump_tick_time,
        'Timestamp ', datetime.now()
    )


while True:
    try:
        pipe = [0]

        while not radio.available(pipe):
            time.sleep(0.01)

        print('radio.available ', pipe, radio.available(pipe))
        receive_buffer = []
        radio.read(receive_buffer)
        val = 0

        if pipe == [1]:
            battery_voltage = (receive_buffer[1] << 8) + receive_buffer[0]
            voltage_in_current_sensing_coil = (receive_buffer[3] << 8) + (receive_buffer[2])
            time_stamp_from_controller = (receive_buffer[7] << 24) + (receive_buffer[6] << 16) + (
                    receive_buffer[5] << 8) + (receive_buffer[4])

            print_heat_pump_tick()
            save_heat_pump_cdr()
            old_heat_pump_tick_time = time_stamp_from_controller

        if pipe == [2]:
            rotation_from_controller = (receive_buffer[3] << 24) + (receive_buffer[2] << 16) + (
                    receive_buffer[1] << 8) + (
                                           receive_buffer[0])
            counter_time_stamp_from_controller = (receive_buffer[7] << 24) + (receive_buffer[6] << 16) + (
                    receive_buffer[5] << 8) + (
                                                     receive_buffer[4])
            time_stamp = datetime.now()
            delta = time_stamp - old_time_stamp

            if 1 == first_run_flag:
                first_run_flag = 0
                old_rotation = rotation_from_controller
                print('first run', old_rotation)

            power = (rotation_from_controller - old_rotation) * 0.01 / (
                    delta.seconds + delta.microseconds / 1000000.) * 3600. * 1000.
            electric_counter_value += (rotation_from_controller - old_rotation) * 0.01

            print_counter_tick()

            publish.single("Electricity", electric_counter_value, hostname="localhost")

            old_counter_tick_time = counter_time_stamp_from_controller
            old_time_stamp = time_stamp
            old_rotation = rotation_from_controller
            save_counter_cdr()
    except KeyboardInterrupt:
        print('finnished')
        break
    except:
        pass

GPIO.cleanup()  # cleanup all GPIO
