# Python3 polling use Adafruit debouncer
import board
import digitalio
import time
from adafruit_debouncer import Debouncer
from datetime import datetime, timedelta
import sys
import paho.mqtt.publish as publish

GAS_TARIF_M3_EUR = 0.36
DEBOUNCING_DELAY_S = 1
v = 0.0
v_init = 0.0
time_stamp_old = datetime.now()
time_stamp = datetime.now()
old_input = 1

if len(sys.argv) == 2:
    print
    'gas counter value:', str(sys.argv[1])
    v = float(sys.argv[1])
    v_init = v


def process_counter_tick():
    global v, time_stamp, time_stamp_old
    v += 0.01
    time_stamp = datetime.now()
    delta_time = time_stamp - time_stamp_old

    volume_hour = 0.01 / (delta_time.seconds + delta_time.microseconds / 1000000.) * 3600.
    power = volume_hour * 9300.
    price_hour = volume_hour * GAS_TARIF_M3_EUR

    print_tick(delta_time, power, price_hour, time_stamp, v, volume_hour)
    publish.single("Gas", v, hostname="localhost")
    save_cdr(power, time_stamp, v, volume_hour)

    time_stamp_old = time_stamp


def print_tick(delta_time, power, price_hour, time_stamp, v, volume_hour):
    print(
        time_stamp, ' ',
        delta_time, ' dt ',
        v, ' m3 ',
        round(v - v_init, 2), ' m3 ',
        round(volume_hour, 2), ' m3/h ',
        round(power, 0), ' w ',
        round(price_hour, 2), ' Eur')


def save_cdr(power, time_stamp, v, volume_hour):
    f = open('gas.cdr', 'a')
    f.write(str(time_stamp) +
            ',' +
            str(v) +
            ',' +
            str(round(v - v_init, 2)) +
            ',' +
            str(round(volume_hour, 2)) +
            ',' +
            str(round(power, 0)) +
            '\n')
    f.close


pin = digitalio.DigitalInOut(board.D27)  # GPIO 27 pin 13
pin.direction = digitalio.Direction.INPUT
pin.pull = digitalio.Pull.DOWN
switch = Debouncer(pin, interval=DEBOUNCING_DELAY_S)

while True:
    try:
        switch.update()
        if switch.rose:
            print('Rose')
        if switch.fell:
            print('Fell')
            process_counter_tick()
        time.sleep(0.2)
    except KeyboardInterrupt:
        print('finished')
        break

GPIO.cleanup()